            #!/bin/bash
            yum -y update
            yum -y install epel-release 

            # install ansible
            yum -y install ansible
            
            # retrieve ansible code
            yum -y install git
            git clone https://github.com/diranetafen/cursus-devops.git
            cd cursus-devops/ansible
            ansible-galaxy install -r roles/requirements.yml
            ansible-playbook install_docker.yml
            sudo usermod -aG docker centos

            # install mysql-server
            sudo yum install wget -y
            wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm
            sudo rpm -ivh mysql-community-release-el7-5.noarch.rpm
            sudo yum update -y
            sudo yum install mysql-server -y
            sudo systemctl start mysqld
