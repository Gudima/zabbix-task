resource "aws_security_group" "zabbix" {
  name        = "zabbix"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.AIOps_vpc.id

  ingress {
    description      = "Zabbix_agent"
    from_port        = 10050
    to_port          = 10050
    protocol         = "tcp"
    cidr_blocks      = [var.subnet_private_cidr_block]
    ipv6_cidr_blocks = [var.subnet_private.ipv6_cidr_block]
  }

  
  ingress {
    description      = "Zabbix_server"
    from_port        = 10051
    to_port          = 10051
    protocol         = "tcp"
    cidr_blocks      = [var.subnet_private_cidr_block]
    ipv6_cidr_blocks = [var.subnet_private.ipv6_cidr_block]
  }

  ingress {
    description      = "Zabbix_Java_gateway"
    from_port        = 10052
    to_port          = 10052
    protocol         = "tcp"
    cidr_blocks      = [var.subnet_private_cidr_block]
    ipv6_cidr_blocks = [var.subnet_private.ipv6_cidr_block]
  }



  ingress {
    description      = "Zabbix_Web_service"
    from_port        = 10053
    to_port          = 10053
    protocol         = "tcp"
    cidr_blocks      = [var.subnet_private_cidr_block]
    ipv6_cidr_blocks = [var.subnet_private.ipv6_cidr_block]
  }


  ingress {
    description      = "Zabbix_frontend"
    from_port        = 443
    to_port          = 443
    protocol         = "https"
    cidr_blocks      = [var.subnet_private_cidr_block]
    ipv6_cidr_blocks = [var.subnet_private.ipv6_cidr_block]
  }

  ingress {
    description      = "Zabbix_frontend2"
    from_port        = 80
    to_port          = 80
    protocol         = "http"
    cidr_blocks      = [var.subnet_private_cidr_block]
    ipv6_cidr_blocks = [var.subnet_private.ipv6_cidr_block]
  }

  egress {
    from_port        = 192.168.0.80
    to_port          = 192.168.0.80
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}
