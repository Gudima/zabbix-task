# Create VPC
resource "aws_vpc" "AIOps_vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "${var.Module_name}.${var.aws_region}"
  }
}
#  Create public subnet
resource "aws_subnet" "AIOps_subnet_public" {
  vpc_id            = aws_vpc.AIOps_vpc.id
  cidr_block        = var.subnet_public_cidr_block
  availability_zone = var.availability_zone
  tags = {
    Name = "${var.Module_name}.${var.aws_region}.${var.availability_zone}.${var.public_subnet_type}"
  }
}
# Create private subnet
resource "aws_subnet" "AIOps_subnet_private" {
  vpc_id            = aws_vpc.AIOps_vpc.id
  cidr_block        = var.subnet_private_cidr_block
  availability_zone = var.availability_zone
  tags = {
    Name = "${var.Module_name}.${var.aws_region}.${var.availability_zone}.${var.private_subnet_type}"
  }
}
# Create gateway
resource "aws_internet_gateway" "AIOps_igw" {
  vpc_id = aws_vpc.AIOps_vpc.id
  tags = {
    Name = "${var.Module_name}.${var.aws_internet_gateway}"
  }
}
# Create public RT
resource "aws_route_table" "AIOps_route_table_public" {
  vpc_id = aws_vpc.AIOps_vpc.id
  route {
    cidr_block = var.route_table_public_cidr_block
    gateway_id = aws_internet_gateway.AIOps_igw.id
  }
  tags = {
    Name = "${var.Module_name}.${var.route_table_public_name}"
  }
}
# Attach public RT
resource "aws_route_table_association" "AIOps_route_table_association_public" {
  subnet_id      = aws_subnet.AIOps_subnet_public.id
  route_table_id = aws_route_table.AIOps_route_table_public.id
}
# Create private RT
resource "aws_default_route_table" "AIOps_route_table_private" {
  default_route_table_id = aws_vpc.AIOps_vpc.default_route_table_id
  tags = {
    Name = "${var.Module_name}.${var.route_table_private_name}"
  }
}
# Attach private RT
resource "aws_route_table_association" "AIOps_route_table_association_private" {
  subnet_id      = aws_subnet.AIOps_subnet_private.id
  route_table_id = aws_default_route_table.AIOps_route_table_private.id
}
# Create S3 bucket for flow logs
resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${lower(var.Module_name)}-${var.aws_s3_bucket_name}"

  tags = {
    Name = "${lower(var.Module_name)}-${var.aws_s3_bucket_name}"
  }
}
# Enable flowlog
resource "aws_flow_log" "vpc_flowlogs" {
  log_destination      = "arn:aws:s3:::${lower(var.Module_name)}-${var.aws_s3_bucket_name}"
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = aws_vpc.AIOps_vpc.id
}
