variable "Module_name" {
  default = "AIOps"
}

variable "aws_internet_gateway" {
  default = "internet_gateway"
}

variable "vpc_cidr_block" {
  default = "10.42.0.0/16"
}

variable "subnet_public_cidr_block" {
  default = "10.42.0.0/24"
}

variable "availability_zone" {
  default = "eu-west-1a"
}

variable "public_subnet_type" {
  default = "public"
}

variable "private_subnet_type" {
  default = "private"
}

variable "subnet_private_cidr_block" {
  default = "10.42.1.0/24"
}

variable "route_table_public_cidr_block" {
  default = "0.0.0.0/0"
}

variable "route_table_public_name" {
  default = "route_table_public"
}

variable "route_table_private_name" {
  default = "route_table_private"
}

variable "aws_s3_bucket_name" {
  default = "flowlogs-bucket"
}
