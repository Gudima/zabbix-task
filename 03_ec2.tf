data "template_file" "script" {
  template = file("script.sh")
}

resource "aws_instance" "zabbix_server" {
  ami           = "ami-033adaf0b583374d4"
  instance_type = "t3.medium"
  subnet_id     = "aws_subnet.AIOps_subnet_private.id"
  user_data_base64 = base64encode(data.template_file.script.rendered)
  vpc_security_group_ids = [aws_security_group.zabbix.id]
}

