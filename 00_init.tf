# Set terraform version
terraform {
  required_version = ">= 0.14"
}
# Initialize providers
provider "aws" {
  region = var.aws_region
}
